package linkedList;

import java.util.Objects;

public class LinkedList<T> {
    protected Node<T> root;

    public void add(T t) {
        if (root == null) {
            root = new Node<T>(t);
        } else {
            Node<T> node = root;
            while (node.getNext() != null) {
                node = node.getNext();
            }
            node.setNext(new Node<T>(t));
        }
    }

    public boolean find(T t) {
        Node<T> node = root;
        while (node != null) {
            if (Objects.equals(t, node.getT())){
                return true;
            }
            node = node.getNext();
        }
        return false;
    }
    public boolean remove(T t){
        if(find(t)) {
            if(Objects.equals(root.getT(),t)){
                root=root.getNext();
                return true;
            }
            Node<T> parent = root;
            Node<T> child = root;
            while (child != null) {
                if(Objects.equals(child.getT(),t)){
                    parent.setNext(child.getNext());
                    return true;
                }
                parent=child;
                child=child.getNext();
            }
        }
        return false;
    }
    public long size(){
        long len=0;
        Node<T> node = root;
        while (node != null) {
            len++;
            node = node.getNext();
        }
        return len;
    }

    @Override
    public String toString() {
        String str = "{  ";
        Node<T> node = root;
        while (node != null) {
            str += node.getT().toString() + ", ";
            node = node.getNext();
        }
        return str.substring(0, str.length() - 2) + "}";
    }
}
