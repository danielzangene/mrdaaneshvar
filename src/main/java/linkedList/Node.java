package linkedList;

public class Node<T> {
    private T t;
    private Node<T> next;

    public Node(T t) {
        this.t = t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public void setNext(Node<T> next) {
        this.next = next;
    }

    public T getT() {
        return t;
    }

    public Node<T> getNext() {
        return next;
    }
}
