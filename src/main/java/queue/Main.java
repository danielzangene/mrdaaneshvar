package queue;

import stack.Stack;

public class Main {
    public static void main(String[] args) {

        Queue<String> s=new Queue<>();
        s.push("a");
        s.push("b");
        s.push("c");
        s.push("d");
        s.push("e");
        s.push("f");
        System.out.println(s);
        s.pop();
        s.pop();
        s.pop();
        System.out.println(s);
        System.out.println(s.empty());
    }
}
