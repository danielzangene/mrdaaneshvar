package queue;

import linkedList.LinkedList;
import linkedList.Node;

public class Queue<T> extends LinkedList<T> {
    public void push(T t) {
        add(t);
    }

    public T pop() {
        if (!empty()) {
            Node<T>node=root;
            root=root.getNext();
            return node.getT();
        }
        return null;
    }

    public boolean empty() {
        if (root == null) {
            return true;
        }
        return false;
    }
}
