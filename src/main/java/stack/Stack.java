package stack;

import linkedList.LinkedList;
import linkedList.Node;

import java.util.Objects;

public class Stack<T> extends LinkedList<T> {
    public void push(T t) {
        add(t);
    }

    public T pop() {
        if (!empty()) {
            if (size() == 1) {
                Node<T> node = root;
                root = null;
                return node.getT();
            }
            Node<T> parent = root;
            Node<T> child = root;
            while (child.getNext() != null) {
                parent = child;
                child = child.getNext();
            }
            parent.setNext(null);
            return child.getT();
        }
        return null;
    }

    public boolean empty() {
        if (root == null) {
            return true;
        }
        return false;
    }
}
